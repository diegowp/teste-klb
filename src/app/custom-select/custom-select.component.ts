import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-custom-select',
  templateUrl: './custom-select.component.html'
})
export class CustomSelectComponent implements OnInit {

  @Input() defaultValue: string;
  @Input() thisSelect: string;
  @Input() listItens: Array<string> = [];

  constructor() { }

  ngOnInit() {
  }

  open(){    
    let select = document.querySelector('#' + this.thisSelect);
    if( select.classList.contains('open') ){
      document.querySelector('div.overlay').classList.remove('open');
      document.querySelector('#' + this.thisSelect).classList.remove('open');
    }else{
      document.querySelector('div.overlay').classList.add('open');
      document.querySelector('#' + this.thisSelect).classList.add('open');
    }
  }
  
  selectItem( item ){
    let select = document.querySelector('#' + this.thisSelect);
    select.querySelector('span.value').innerHTML = item;
  }

}
