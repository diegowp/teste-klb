import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MgmtPanelComponent } from './mgmt-panel/mgmt-panel.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/painel-de-gestao',
    pathMatch: 'full'
  },
  {
    path: 'painel-de-gestao',
    component: MgmtPanelComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
