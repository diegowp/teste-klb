import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MgmtPanelComponent } from './mgmt-panel.component';

describe('MgmtPanelComponent', () => {
  let component: MgmtPanelComponent;
  let fixture: ComponentFixture<MgmtPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MgmtPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MgmtPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
