import { Component, OnInit } from '@angular/core';
import { productsService } from '../services/products-service.service';

@Component({
  selector: 'app-mgmt-panel',
  templateUrl: './mgmt-panel.component.html'
})
export class MgmtPanelComponent implements OnInit {

  defaultActions: Array<string> = [
    'Detalhes'
  ];

  products: Array<string> = [];

  constructor( private productService: productsService ) { }

  ngOnInit() {

    this.productService.getAll().subscribe(data => {
      this.products = data as Array<any>;
    }, err => {
      this.products = [
        'Produto 1',
        'Produto 2'
      ];
    });

  }

}
